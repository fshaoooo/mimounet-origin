import argparse
from torch.backends import cudnn
from models.MIMOUNet import MIMOUNet
from train import _train
'''
params

--lr            学习率
--weight_decay  权值衰减
--data_dir      数据集位置
--batch_size    batchsize
--num_worker    读取进程数量
--lr_steps      每多少step改一次学习率
--gamma         学习率乘上gamma
--load_param    加载第几个参数
--save_path     保存文件路径

'''
if __name__ == "__main__":
    cudnn.benchmark = True

    parser = argparse.ArgumentParser()
    parser.add_argument("--lr", type=float, default=1e-4)
    parser.add_argument('--data_dir', type=str, default='../../DataSet/GOPRO')
    parser.add_argument('--batch_size', type=int, default=4)
    parser.add_argument('--num_worker', type=int, default=8)
    parser.add_argument('--lr_steps', type=list, default=[(x + 1) * 500 for x in range(3000 // 500)])
    parser.add_argument('--gamma', type=float, default=0.5)
    parser.add_argument('--load_param', type=int, default=0)
    parser.add_argument("--save_path", type=str, default='./output')
    parser.add_argument('--weight_decay', type=float, default=0)
    parser.add_argument('--num_epoch', type=int, default=20)
    parser.add_argument("--summary_name", type=str, default="runs/MIMOUNet")
    parser.add_argument("--log_name", type=str, default="MIMOUNet.log")

    args = parser.parse_args()
    model = MIMOUNet()
    _train(model, args)


