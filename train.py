import os
import torch

from data import train_dataloader
from utils import Adder
from tensorboardX import SummaryWriter
import torch.nn.functional as F
from valid import _valid

'''
params

--lr            学习率
--weight_decay  权值衰减
--data_dir      数据集位置
--batch_size    batchsize
--num_worker    读取进程数量
--lr_steps      每多少step改一次学习率
--gamma         学习率乘上gamma
--load_param    加载第几个参数
--save_path     保存文件路径

'''

def MSFRLoss(input_tensor, target_tensor):
    label_fft1 = torch.fft.fft2(target_tensor, dim=(-2, -1))
    label_fft1 = torch.stack((label_fft1.real, label_fft1.imag), -1)

    pred_fft1 = torch.fft.fft2(input_tensor, dim=(-2, -1))
    pred_fft1 = torch.stack((pred_fft1.real, pred_fft1.imag), -1)
    return F.l1_loss(pred_fft1, label_fft1)

def _train(model, args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)
    criterion = torch.nn.L1Loss()
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=args.lr,
                                 weight_decay=args.weight_decay)
    dataloader = train_dataloader(args.data_dir, args.batch_size, args.num_worker)
    scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, args.lr_steps, args.gamma)
    epoch = 1

    model_save_path = os.path.join(args.save_path, "model")
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path)
################################## 参数加载#########################################
    if args.load_param > 0:
        state = torch.load(os.path.join(model_save_path, "model_%d.pth" % args.load_param))
        epoch = args.load_param
        epoch += 1
        optimizer.load_state_dict(state["optimizer"])
        scheduler.load_state_dict(state["scheduler"])
        model.load_state_dict(state['model'])
        print("加载成功： %d epoch 参数" % args.load_param)

    writer = SummaryWriter(args.summary_name)
    epoch_pixel_adder = Adder()
    epoch_fft_adder = Adder()
    best_psnr = -1
    stopEpoch = epoch + args.num_epoch + 1

    for epoch_idx in range(epoch, stopEpoch):
        for step, data in enumerate(dataloader):
            image, target = data[0].to(device), data[1].to(device)

            optimizer.zero_grad()
            preds = model(image)

            targetX2 = F.interpolate(target, scale_factor=0.5, mode="bilinear", align_corners=True, recompute_scale_factor=True)
            targetX4 = F.interpolate(target, scale_factor=0.25, mode="bilinear", align_corners=True, recompute_scale_factor=True)

            l1 = criterion(preds[0], targetX4)
            l2 = criterion(preds[1], targetX2)
            l3 = criterion(preds[2], target)
            loss_content = l1 + l2 + l3

            f1 = MSFRLoss(preds[0], targetX4)
            f2 = MSFRLoss(preds[1], targetX2)
            f3 = MSFRLoss(preds[2], target)
            loss_fft = f1 + f2 + f3

            loss = loss_content + 0.1 * loss_fft
            loss.backward()
            optimizer.step()

            epoch_fft_adder(loss_fft.item())
            epoch_pixel_adder(loss_content.item())
        ## 完成一个epoch记录参数和loss
        torch.save({
            "optimizer": optimizer.state_dict(),
            "scheduler": scheduler.state_dict(),
            "model": model.state_dict()
        }, os.path.join(model_save_path, "model_%d.pth" % epoch_idx))
        print("Epoch: %d\n pixel Loss: %7.4f FFT Loss: %7.4f" % (epoch_idx,
                                                                 epoch_pixel_adder.average(),
                                                                 epoch_fft_adder.average()))
        writer.add_scalar("Content Loss", epoch_pixel_adder.average(), global_step=epoch_idx)
        writer.add_scalar("FFT Loss", epoch_fft_adder.average(), global_step=epoch_idx)

        epoch_pixel_adder.reset()
        epoch_fft_adder.reset()
        scheduler.step()
        ### valid
        val_gopro = _valid(model, args, epoch_idx)
        print("%d epoch\n Average GOPRO PSNR %.2f db"%(epoch_idx, val_gopro))
        writer.add_scalar("PSNR_GOPRO", val_gopro, epoch_idx)
        if val_gopro >= best_psnr:
            torch.save({"model": model.state_dict()}, os.path.join(model_save_path, "Best.pth"))









