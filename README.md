# MIMOUNet-origin

#### 介绍
修改MIMOUNet源码，只是修改那些pytorch旧版和新版差异部分。让新版pytorch能够正常运行而已。train文件也是从源train文件自己重新打了一遍，其中去掉了一些自己不太需要的指标记录，以及一些小修改。有需要的可以自己添加上。valid文件中添加了保存每个epoch训练之后用valid产生的清晰图。存储不够的同学记得删掉保存图片的代码哈。。。。

#### 目录结构
mimounet-origin
├── data									数据集相关，复制的源码的。
│   ├── data_augment.py		数据增强，相同操作作用在blur,sharp图上
│   ├── data_load.py				Dataloader
│   └── preprocessing.py		整理GOPRO数据集用的
├── main.py								设定输入的参数，传入train中训练
├── models								模型文件
│   ├── MIMOUNet.py
│   └── layers.py
├── train.py							训练
├── utils.py								工具，就是计算指标平均值用的
└── valid.py							验证

#### 使用说明

```python
python main.py --save_path ./output --data_dir ../DataSet/GOPRO --num_epoch 100 --batch_size 8
```

```python
'''
params

--lr            学习率
--weight_decay  权值衰减
--data_dir      数据集位置
--batch_size    batchsize
--num_worker    读取进程数量
--lr_steps      每多少step改一次学习率
--gamma         学习率乘上gamma
--load_param    加载第几个参数
--save_path     保存文件路径

'''
```

