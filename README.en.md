# MIMOUNet-origin

#### Description
修改MIMOUNet源码，只是修改那些pytorch旧版和新版差异部分。让新版pytorch能够正常运行而已。train文件也是从源train文件自己重新打了一遍，其中去掉了一些自己不太需要的指标记录，以及一些小修改。有需要的可以自己添加上。valid文件中添加了保存每个epoch训练之后用valid产生的清晰图。存储不够的同学记得删掉保存图片的代码哈。。。。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
